enum Enemy {
	Bat;
	Beetle;
	Jelly;
	Rat;
	Slime;
	Snake;
	Spider;
}

class Member {
	public final enemy:Enemy;
	public final level:Int;

	public var str:Float;
	public var dex:Float;
	public var con:Float;
	public var wis:Float;

	public function new(enemy:Enemy, level:Int) {
		this.enemy = enemy;
		this.level = level;
		switch (enemy) {
			case Bat:
				str = Math.max(level / 2, 2);
				dex = level * 1.5;
				wis = level / 2;
				con = Math.max(level, 2);
			case Beetle:
				str = Math.max(level - 1, 2);
				dex = level / 2 + 1;
				wis = level / 4;
				con = Math.max(level, 2);
			case Jelly:
				str = level;
				dex = level / 4 + 1;
				wis = 1;
				con = level * 2 - level / 4;
			case Rat:
				str = Math.max(level - 3, 1);
				dex = level / 2;
				wis = level / 4;
				con = Math.max(level - 2, 1);
			case Slime:
				str = level / 2;
				dex = (level / 3) + 1;
				wis = 1;
				con = level * 2;
			case Snake:
				str = Math.max(level * 0.75, 2);
				dex = level;
				wis = level / 2;
				con = level;
			case Spider:
				str = Math.max(level, 2);
				dex = level / 2 + 1;
				wis = level / 4;
				con = Math.max(level - 1, 3);
		}
	}

	public var maxHealth(get, null):Float;

	function get_maxHealth():Float
		return con * 2;
}
