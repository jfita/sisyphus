import h2d.ScaleGrid;

class Button extends h2d.Object {
	final background:h2d.ScaleGrid;

	public function new(text:String, font:h2d.Font, style:String, ?parent:h2d.Object) {
		super(parent);

		var caption = new h2d.Text(font);
		caption.text = text;
		caption.textColor = 0xff333333;
		final width = caption.calcTextWidth(text);

		var pressButton = hxd.Res.tiny_dungeon_interface.get('button_${style}_press');
		final pressed = new h2d.ScaleGrid(pressButton, 3, 3, 3, 3, this);
		pressed.width = width + 20;

		var backgroundButton = hxd.Res.tiny_dungeon_interface.get('button_${style}');
		background = new h2d.ScaleGrid(backgroundButton, 3, 3, 3, 3, this);
		background.width = width + 20;

		addChild(caption);
		caption.setPosition(10, background.height / 2 - 3);

		final interactive = new h2d.Interactive(this.width, this.height, this);
		interactive.onPush = event -> background.visible = false;
		interactive.onRelease = event-> background.visible = true;
		interactive.onClick = event -> onClick();
	}

	public dynamic function onClick() {
	}

	public var height(get, null):Float;
	public var width(get, null):Float;

	function get_height():Float
		return background.height;

	function get_width():Float
		return background.width;
}
