using DungeonGenerator.RandExtender;

class Fighter extends h2d.Object {
	final background:h2d.ScaleGrid;
	final member:Party.Member;
	final health:HealthBar;
	final font:h2d.Font;

	public function new(fight:Fight, member:Party.Member, width:Float, font:h2d.Font, ?parent:h2d.Object) {
		super(parent);

		this.member = member;
		this.font = font;
		final height = 32;

		final pressButton = hxd.Res.tiny_dungeon_interface.get('button_stone_press');
		final pressed = new h2d.ScaleGrid(pressButton, 3, 3, 3, 3, this);
		pressed.width = width;
		pressed.height = height;

		var backgroundButton = hxd.Res.tiny_dungeon_interface.get('button_stone');
		background = new h2d.ScaleGrid(backgroundButton, 3, 3, 3, 3, this);
		background.width = width;
		background.height = height;

		final window = hxd.Res.tiny_dungeon_interface.get("panel_window");
		final windowGrid = new h2d.ScaleGrid(window, 6, 6, 6, 6, this);
		windowGrid.width = windowGrid.height = 28;
		windowGrid.setPosition(5, height / 2 - windowGrid.height / 2);

		final monsters = hxd.Res.tiny_dungeon_monsters;
		final frames = monsters.getAnim('${Std.string(member.enemy).toLowerCase()}_d');
		final anim = new h2d.Anim(frames, Game.AnimSpeed, windowGrid);
		anim.setPosition(6, 6);

		var name = new h2d.Text(font, this);
		name.text = Std.string(member.enemy);
		name.textColor = 0xff333333;
		name.setPosition(windowGrid.x + windowGrid.width + 5, background.height / 2 - 3);

		health = new HealthBar(this);
		health.value = health.maxValue = member.maxHealth;
		health.setPosition(width - health.width - 5, height / 2 - health.height / 2);

		final interactive = new h2d.Interactive(background.width, background.height, this);
		interactive.onPush = event -> background.visible = false;
		interactive.onRelease = event -> background.visible = true;
		interactive.onClick = event -> {
			fight.attack(this);
		};
	}

	public var str(get, null):Float;
	public var dex(get, null):Float;
	public var level(get, null):Int;
	public var dead(get, null):Bool;

	function get_str():Float
		return member.str;

	function get_dex():Float
		return member.dex;

	function get_level():Int
		return member.level;

	function get_dead():Bool
		return health.dead;

	public var expectedDamage(null, set):Float;

	function set_expectedDamage(dmg:Float):Float {
		return health.expectedDamage = dmg;
	}

	public function takeDamage(damage:Float) {
		hxd.Res.impact_a.play();
		health.value -= damage;
		addLabel(Std.string(damage));
	}

	public function miss() {
		hxd.Res.miss.play();
		addLabel("Miss");
	}

	function addLabel(t:String) {
		var label = new Label.Label(font, this);
		label.text = t;
		label.setPosition(0, 3);
		label.maxWidth = background.width;
		label.textAlign = Center;
	}
}

class Fight extends h2d.Object {
	final font:h2d.Font;
	final rand:hxd.Rand;
	final sisyphus:Sisyphus;
	final fighters:Array<Fighter> = new Array();

	public function new(sisyphus:Sisyphus, party:Array<Party.Member>, font:h2d.Font, rand:hxd.Rand, ?parent:h2d.Object) {
		super(parent);

		this.font = font;
		this.sisyphus = sisyphus;
		this.rand = rand;

		setPosition(3, 3);

		var backgroundScroll = hxd.Res.tiny_dungeon_interface.get("panel_stone");
		var background = new h2d.ScaleGrid(backgroundScroll, 16, 16, 16, 16, this);
		background.width = Game.WorldWidth * Game.TileWidth - 6;
		background.height = Game.WorldHeight * Game.TileHeight - 48 - 3;

		final fighterWidth = background.width - 10;
		var y = background.height - 10 - 35;
		sisyphus.expectedDamage = 0;
		for (member in party) {
			final fighter = new Fighter(this, member, fighterWidth, font, this);
			fighter.setPosition(background.x + background.width / 2 - fighterWidth / 2, y);
			fighter.expectedDamage = sisyphus.str;
			y -= 35;
			fighters.push(fighter);
			sisyphus.expectedDamage += fighter.str;
		}
	}

	public function attack(fighter:Fighter) {
		if (rand.range(1, 20) + sisyphus.dex > rand.range(1, 20) + fighter.dex) {
			fighter.takeDamage(sisyphus.str);
			if (fighter.dead) {
				hxd.Res.sewers_mob_dies.play();
				fighters.remove(fighter);
				fighter.remove();
				sisyphus.experience += Std.int(10 * fighter.level / sisyphus.level);
			}
		} else {
			fighter.miss();
		}
		var y = 0;
		for (fighter in fighters) {
			if (rand.range(1, 20) + fighter.dex > rand.range(1, 20) + sisyphus.dex) {
				takeDamage(fighter.str, y);
			} else {
				miss(y);
			}
			y++;
		}
		sisyphus.expectedDamage = 0;
		if (fighters.length == 0 || sisyphus.dead) {
			onClose();
		} else {
			for (fighter in fighters) {
				fighter.expectedDamage = sisyphus.str;
				sisyphus.expectedDamage += fighter.str;
			}
		}
	}

	function takeDamage(damage:Float, y:Int) {
		hxd.Res.impact_b.play();
		sisyphus.health.value -= damage;
		addLabel(Std.string(damage), y);
	}

	function miss(y:Int) {
		hxd.Res.miss.play();
		addLabel("Miss", y);
	}

	function addLabel(t:String, y:Int) {
		var label = new Label.Label(font, sisyphus.portrait);
		label.text = t;
		label.setPosition(0, 3 + y * 8);
		label.maxWidth = sisyphus.portrait.width;
		label.textAlign = Center;
	}

	public dynamic function onClose() {}
}
