using DungeonGenerator.RandExtender;

class Sisyphus {
	public var level:Int = 1;
	public var str:Float = 3;
	public var dex:Float = 2;
	public var con:Float = 4;
	public var wis:Float = 1;
	public var health:HealthBar = new HealthBar();
	public var portrait:Portrait = new Portrait();

	final rand:hxd.Rand;

	public function new(rand:hxd.Rand) {
		this.rand = rand;
		health.maxValue = maxHealth;
		health.value = maxHealth;
	}

	public var expectedDamage(get, set):Float;
	public var maxHealth(get, null):Float;
	public var dead(get, null):Bool;
	public var experience(get, set):Int;

	function get_maxHealth():Float
		return con * 2;

	function get_expectedDamage():Float
		return health.expectedDamage;

	function set_expectedDamage(dmg:Float):Float {
		return health.expectedDamage = dmg;
	}

	function get_dead():Bool
		return health.dead;

	function get_experience():Int
		return portrait.experience;

	function set_experience(xp:Int):Int {
		portrait.experience = xp;
		while (portrait.experience >= 100) {
			hxd.Res.level_up.play();
			level++;
			portrait.experience -= 100;

			if (rand.randBoolean(0.80)) {
				str += rand.range(1, 2);
			}
			if (rand.randBoolean(0.65)) {
				dex += rand.range(1, 2);
			}
			if (rand.randBoolean(0.15)) {
				wis += 1;
			}
			con += 2;
			health.value = health.maxValue = maxHealth;
		}
		return portrait.experience;
	}

	public function reset() {
		level = 1;
		str = 3;
		dex = 2;
		con = 4;
		wis = 1;
		health.value = health.maxValue = maxHealth;
		portrait.experience = 0;
	}
}
